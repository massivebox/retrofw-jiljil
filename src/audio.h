void play_sound(char *name);
void play_sound_and_wait(char *name);
void play_bgm(char *name);

void pause_bgm(void);
void resume_bgm(void);
void stop_bgm(void);

void load_audio(void);
void unload_audio(void);