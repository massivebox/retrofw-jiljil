#include "include.h"

int internal_keycode_from_sdl(int sdl_keycode) {

    /*
    invalid = -1
    up = 0
    right = 1
    down = 2
    left = 3
    start = 4
    select = 5
    */


    switch(sdl_keycode) {
        case SDLK_ESCAPE: //Select
            return 5;
        case SDLK_RETURN:
            return 4;
        case SDLK_LCTRL: // A button
        case SDLK_RIGHT: // right
            return 1;
        case SDLK_LSHIFT: // Y button
        case SDLK_LEFT: // left
            return 3;
        case SDLK_LALT: // B button
        case SDLK_DOWN: // down
            return 2;
        case SDLK_SPACE: // X button
        case SDLK_UP: // up
            return 0;
        default:
            return -1;
    }

}
