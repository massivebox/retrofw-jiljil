#include "include.h"

//#include "main.h"
#include "audio.h"
#include "match.h"
#include "graphics.h"

SDL_Surface *tilesIMG;
SDL_Surface *fpsFontIMG;

int main(void) {

    // attempt to initialize graphics and timer systema
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER|SDL_INIT_AUDIO) != 0) {
        printf("error initializing SDL: %s\n", SDL_GetError());
        return 1;
    }

    atexit(SDL_Quit);

    SDL_Surface * screen = SDL_SetVideoMode(320, 240, 16, SDL_SWSURFACE);
    if (!screen) {
        printf("error creating window: %s\n", SDL_GetError());
        SDL_Quit();
	    return 1;
    }
    SDL_ShowCursor(SDL_DISABLE);

    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) != 0) {
        printf("error initializing SDL Mixer: %s\n", Mix_GetError());
    }
    load_audio();

    tilesIMG = load_asset("assets/images/JILJIL.png");
    fpsFontIMG = load_asset("assets/images/fpsfont.png");

    int quit = 0;
    quit = boot_animation(screen);
    if(!quit) {
        while(!quit) {
            quit = match(screen);
        }
    }

    SDL_Quit();
    unload_audio();
    Mix_Quit();

}
