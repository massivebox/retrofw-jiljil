#include "include.h"

#include "main.h"

extern SDL_Surface *tilesIMG;
extern SDL_Surface *fpsFontIMG;

// load_asset is a helper function to reload assets from a file
SDL_Surface* load_asset(char path[]) {

    SDL_Surface* _img = IMG_Load(path);
	SDL_Surface* img = SDL_DisplayFormat(_img);
	SDL_FreeSurface(_img);

    return img;

}

// print_score draws the specified score on the specified screen at the specified coordinates
void print_score(int score_x, int score_y, int number, SDL_Surface * screen) {

    int n = log10(number) + 1;
    int i;
    int *numberArray = calloc(n, sizeof(int));
    for ( i = 0; i < n; ++i, number /= 10 )
    {
        numberArray[i] = number % 10;
    }

    SDL_Rect SrcZero = {x: 0, y: 112, w: 8, h: 16};
    SDL_Rect DstDigitZero = {x: score_x, y: score_y};
    SDL_Rect DstDigitOne = {x: score_x + 8, y: score_y};
    SDL_Rect DstDigitTwo = {x: score_x + 16, y: score_y};

    if(n == 1) {
        SDL_BlitSurface(tilesIMG, &SrcZero, screen, &DstDigitZero);
        SDL_BlitSurface(tilesIMG, &SrcZero, screen, &DstDigitOne);
        SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 8 * numberArray[0], y: 112, w: 8, h: 16}, screen, &DstDigitTwo);
    } else if(n == 2) {
        SDL_BlitSurface(tilesIMG, &SrcZero, screen, &DstDigitZero);
        SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 8 * numberArray[0], y: 112, w: 8, h: 16}, screen, &DstDigitTwo);
        SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 8 * numberArray[1], y: 112, w: 8, h: 16}, screen, &DstDigitOne);
    } else if(n == 3) {
        SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 8 * numberArray[0], y: 112, w: 8, h: 16}, screen, &DstDigitTwo);
        SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 8 * numberArray[1], y: 112, w: 8, h: 16}, screen, &DstDigitOne);
        SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 8 * numberArray[2], y: 112, w: 8, h: 16}, screen, &DstDigitZero);
    } else { // for zero digits and more than 3 we just show zeroes lol
        SDL_BlitSurface(tilesIMG, &SrcZero, screen, &DstDigitTwo);
        SDL_BlitSurface(tilesIMG, &SrcZero, screen, &DstDigitOne);
        SDL_BlitSurface(tilesIMG, &SrcZero, screen, &DstDigitZero);
    }

}

// clear_screen resets the screen
void clear_screen(SDL_Surface* screen) {

    SDL_FillRect(screen, NULL, 0);

    int row, col;
    for (row = 0; row < 2; row++) {
        for (col = 0; col < 14; col++) {
            SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 64, y: 64, w: 20, h: 20}, screen, &(SDL_Rect){x: 21 + 20*col, y: row*221});
        }
    }

	SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 80, y: 112, w: 17, h: 7}, screen, &(SDL_Rect){x: 208, y: 22}); //Hi
	SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 80, y: 121, w: 47, h: 7}, screen, &(SDL_Rect){x: 226, y: 22}); //Score
	
	SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 80, y: 121, w: 47, h: 7}, screen, &(SDL_Rect){x: 226, y: 213}); //Score
	
	SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 64, y: 95, w: 23, h: 7}, screen, &(SDL_Rect){x: 22, y: 213}); //JpLeft
	SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 64, y: 84, w: 65, h: 8}, screen, &(SDL_Rect){x: 49, y: 212}); //JpRight
	
	SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 80, y: 16, w: 48, h: 7}, screen, &(SDL_Rect){x: 141, y: 224}); //exit->esc

}
