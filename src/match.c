#include "include.h"

#include "main.h"
#include "audio.h"
#include "logic.h"
#include "graphics.h"
#include "keys.h"

int pause_game(SDL_Surface *screen) {

    pause_bgm();
    //return 1 = quit; return 0 = proceed
    SDL_Surface *pauseIMG = load_asset("assets/images/pause.png");

    SDL_BlitSurface(pauseIMG, NULL, screen, NULL);
    SDL_Flip(screen);

    SDL_FreeSurface(pauseIMG);

    SDL_Event event;
    while (1) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                return 1;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case SDLK_ESCAPE: //select -> exit
                    return 1;
                case SDLK_RETURN: //start -> continue
                    resume_bgm();
                    return 0;
                case SDLK_BACKSPACE: //R -> reset
                    resume_bgm();
                    return 2;
                case SDLK_LSHIFT: //Y -> clear best score
                    save_best_score_to_file(0);
                    resume_bgm();
                    return 2;
                case SDLK_SPACE: //X -> zen mode
                    // to be implemented soon (TM)
                    break;
                case SDLK_LALT: //B -> hard mode
                    // to be implemented soon (TM)
                    break;
                case SDLK_LCTRL: //A -> normal
                    // to be implemented soon (TM)
                    break;
                default:
                    break;
                }
            }
        }
        SDL_Delay(100);
    }

}

int match(SDL_Surface *screen) {

    //return 1 = quit; return 0 = new game

    int game_over = 0;

    int player_x = 155;
    int player_y = 213;
    float player_speed_x = 0;
    float player_speed_y = 0;

    int player_last_x_1 = 0;
    int player_last_y_1 = 0;
    int paw1_x = 0;
    int paw1_y = 0;
    int player_last_x_2 = 0;
    int player_last_y_2 = 0;
    int paw2_x = 0;
    int paw2_y = 0;

    int lemon_x = 0;
    int lemon_y = 0;
    float lemon_speed_x = 2.5;
    float lemon_speed_y = 2.5;

    int down_keys[6] = {0};
    int score = 0;
    int best_score = get_best_score_from_file();

    int player_past_x[40] = {0};
    int player_past_y[40] = {0};

    long long frame = 0;
    long long last_point = 0;

    play_sound("game_start");
    stop_bgm();
    play_bgm("in_game");
    while(!game_over) {


        frame++;
        Uint32 start = SDL_GetTicks();
        SDL_Event event;
		
		// add or remove keys to the down keys array
        int key = 0;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                return 1;
            case SDL_KEYDOWN:
                key = internal_keycode_from_sdl(event.key.keysym.sym);
                if(key != -1) {
                    down_keys[key] = 1;
                }
                break;
            case SDL_KEYUP:
                key = internal_keycode_from_sdl(event.key.keysym.sym);
                if(key != -1) {
                    down_keys[key] = 0;
                }
                break;
            }
        }

        player_decelerate(&player_speed_x, &player_speed_y);
        player_cap(&player_speed_x, &player_speed_y);
        player_approximate(&player_speed_x, &player_speed_y);
		avoid_player_outside_bonds(&player_x, &player_y, &player_speed_x, &player_speed_y);

		// add speed according to down keys
        if(down_keys[0]) {
            player_speed_y -= 0.2;
        }
        if(down_keys[1]) {
            player_speed_x += 0.2;
        }
        if(down_keys[2]) {
            player_speed_y += 0.2;
        }
        if(down_keys[3]) {
            player_speed_x -= 0.2;
        }
		
		// add player speed to player position
        player_x += player_speed_x;
        player_y += player_speed_y;
        
        
        lemon_decelerate(&lemon_speed_x, &lemon_speed_y);
        lemon_cap(&lemon_speed_x, &lemon_speed_y);
        avoid_lemon_outside_bonds(&lemon_x, &lemon_y, &lemon_speed_x, &lemon_speed_y);
		
		// add lemon speed to lemon position
        lemon_x += lemon_speed_x;
        lemon_y += lemon_speed_y;
        
        if (is_player_inside_lemon(player_x, player_y, lemon_x, lemon_y)) {
            lemon_speed_x = lemon_speed_x + player_speed_x;
            lemon_speed_y = lemon_speed_y + player_speed_y;
            player_speed_x = -player_speed_x * 0.9;
            player_speed_y = -player_speed_y * 0.9;
            if(last_point + 30 < frame) {
                score++;
                play_sound("clap");
                last_point = frame;
            }
		}
        
        clear_screen(screen);

        // update paw prints
        if(frame % 30 == 0) {
            if((frame / 30) % 2 == 0) {
                player_last_x_1 = player_past_x[0];
                player_last_y_1 = player_past_y[0];
                paw1_x = player_last_x_2;
                paw1_y = player_last_y_2;
            }else{
                player_last_x_2 = player_past_x[0];
                player_last_y_2 = player_past_y[0];
                paw2_x = player_last_x_1;
                paw2_y = player_last_y_1;
            }
        }

        // TEST
        // SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 31, y: 79, w: 5, h: 5}, screen, &(SDL_Rect){x: player_past_x[5], y: player_past_y[5]});

        if(check_player_inside_prints(player_past_x[5], player_past_y[5], paw1_x, paw1_y, paw2_x, paw2_y)) {
            if(frame > 180) {
                game_over = 1;
            }
        }
        
        // draw lemon
		SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 0, y: 48, w: 64, h: 64}, screen, &(SDL_Rect){x: lemon_x, y: lemon_y});
        
        // add position to past positions and draw tail pieces
        int *temp_past_x;
        temp_past_x = player_past_x;
        int *temp_past_y;
        temp_past_y = player_past_y;
        int i;
		for(i = 0; i < 39; i++) {
			player_past_x[i] = temp_past_x[i+1];
			player_past_y[i] = temp_past_y[i+1];
			if(i % 5 == 0 && player_past_x[i] != 0 && player_past_y[i] != 0) {
				// draw player tail pieces
				SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 128 - i/5 * 16, y: 0, w: 16, h: 16}, screen, &(SDL_Rect){x: player_past_x[i], y: player_past_y[i]});
			}
		}
		player_past_x[39] = player_x;
		player_past_y[39] = player_y;
		
		// draw player head
        SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 0, y: 0, w: 16, h: 16}, screen, &(SDL_Rect){x: player_x, y: player_y});
		
        // add paw prints
        if(paw1_x != 0) { // make sure it isn't in the first secs of the game where random prints would appear in top left
            SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 0, y: 16, w: 32, h: 32}, screen, &(SDL_Rect){x: paw1_x, y: paw1_y});
            SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 32, y: 16, w: 32, h: 32}, screen, &(SDL_Rect){x: paw2_x, y: paw2_y});
        }

		// handle pause input
        int was_paused = 0;
        if(down_keys[4] || down_keys[5]) {
            switch(pause_game(screen)) {
                case 1: // request to exit
                    return 1;
                case 2: // restart the game
                    return 0;
            }
            was_paused = 1;
            down_keys[4] = 0;
            down_keys[5] = 0;
        }

        print_score(276, 204, score, screen); // current score
        print_score(276, 21, best_score, screen); // best score


        SDL_Flip(screen);
		
		// sleep so that the fps stays at abt 60fps
        if(!was_paused) {
			
            Uint32 end = SDL_GetTicks();
            float elapsedMS = end - start;
            float sleepFor = floor(16.666f - elapsedMS);

            if(sleepFor < 0) {
                sleepFor = 0; // just to be safe
            }

            SDL_Delay(sleepFor);

        }

    }

    stop_bgm();
    play_bgm("game_over");

    if(score > best_score) {
        save_best_score_to_file(score);
    }

    SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 64, y: 32, w: 64, h: 16}, screen, &(SDL_Rect){x: 133, y: 206}); // push start
    SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 64, y: 16, w: 16, h: 16}, screen, &(SDL_Rect){x: player_x, y: player_y}); // caught face
    SDL_Flip(screen);

    SDL_Event event;
    while (1) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                return 1;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case SDLK_ESCAPE: //select -> pause menu
                    if(pause_game(screen) == 1){
                        return 1;
                    }
                case SDLK_RETURN: //start -> new game
                    return 0;
                default:
                    break;
                }
            }
        }
        SDL_Delay(100);
    }

    return 0;

}

int boot_animation(SDL_Surface *screen) {

    //return 1 = quit 0 = continue;

    SDL_Event event;
    SDL_Flip(screen);

    play_sound_and_wait("inhale");
    SDL_Rect SrcR = {x: 89, y: 64, w: 35, h: 20};
    SDL_Rect DestR = {x: 126, y: 51};
    SDL_BlitSurface(tilesIMG, &SrcR, screen, &DestR); //Jil
    SDL_Flip(screen);

    DestR.x = 126+35;
    SDL_BlitSurface(tilesIMG, &SrcR, screen, &DestR); //Jil
    SDL_Flip(screen);

    play_sound_and_wait("sneeze_long");

    SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 80, y: 25, w: 44, h: 7}, screen, &(SDL_Rect){x: 138, y: 121}); //Date
    SDL_Flip(screen);

    play_sound_and_wait("sneeze_short");

    SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 64, y: 103, w: 55, h: 9}, screen, &(SDL_Rect){x: 133, y: 141}); //Tortoiseshell
    SDL_Flip(screen);
    //SDL_Delay(1000);

    play_sound_and_wait("cough_cough");
    while (SDL_PollEvent(&event)) {
        // discard the inputs
    }

    SDL_BlitSurface(tilesIMG, &(SDL_Rect){x: 66, y: 37, w: 62, h: 10}, screen, &(SDL_Rect){x: 133, y: 166}); //Push start
    SDL_Flip(screen);

    play_sound("ronf");

    while (1) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                return 1;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case SDLK_ESCAPE: //select -> pause menu
                    if(pause_game(screen)) {
                        return 1;
                    }
                case SDLK_RETURN: //start -> continue
                    return 0;
                default:
                    break;
                }
            }
        }
        SDL_Delay(100);
    }

}
