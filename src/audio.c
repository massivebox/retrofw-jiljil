#include "include.h"

#include "audio.h"

// sfx
Mix_Chunk *bongo;
Mix_Chunk *cat_sound;
Mix_Chunk *cho;
Mix_Chunk *clap;
Mix_Chunk *cough_cough;
Mix_Chunk *game_start;
Mix_Chunk *inhale;
Mix_Chunk *reverse;
Mix_Chunk *ronf;
Mix_Chunk *rumble;
Mix_Chunk *sneeze_long;
Mix_Chunk *sneeze_short;

// bgm
Mix_Chunk *game_over;
Mix_Chunk *in_game;

int bgm_channel = 0;

void load_audio(void) {

	bongo = Mix_LoadWAV("assets/sfx/bongo");
	cat_sound = Mix_LoadWAV("assets/sfx/cat_sound");
	cho = Mix_LoadWAV("assets/sfx/cho");
 	clap = Mix_LoadWAV("assets/sfx/clap");
 	cough_cough = Mix_LoadWAV("assets/sfx/cough_cough");
 	game_start = Mix_LoadWAV("assets/sfx/game_start");
 	inhale = Mix_LoadWAV("assets/sfx/inhale");
	reverse = Mix_LoadWAV("assets/sfx/reverse");
	ronf = Mix_LoadWAV("assets/sfx/ronf");
	rumble = Mix_LoadWAV("assets/sfx/rumble");
	sneeze_long = Mix_LoadWAV("assets/sfx/sneeze_long");
	sneeze_short = Mix_LoadWAV("assets/sfx/sneeze_short");

	game_over = Mix_LoadWAV("assets/bgm/game_over");
	in_game = Mix_LoadWAV("assets/bgm/in_game");

}

void unload_audio(void) {
	
	Mix_FreeChunk(bongo);
	Mix_FreeChunk(cat_sound);
	Mix_FreeChunk(cho);
 	Mix_FreeChunk(clap);
 	Mix_FreeChunk(cough_cough);
 	Mix_FreeChunk(game_start);
 	Mix_FreeChunk(inhale);
	Mix_FreeChunk(reverse);
	Mix_FreeChunk(ronf);
	Mix_FreeChunk(rumble);
	Mix_FreeChunk(sneeze_long);
	Mix_FreeChunk(sneeze_short);

	Mix_FreeChunk(game_over);
	Mix_FreeChunk(in_game);
	
}

int play_sound_internal(char *name, int loop) {

	Mix_Chunk *sound;

	if(!strcmp(name, "bongo")) {
		sound = bongo;
	} else if (!strcmp(name, "cat_sound")) {
		sound = cat_sound;
	} else if (!strcmp(name, "cho")) {
		sound = cho;
	} else if (!strcmp(name, "clap")) {
		sound = clap;
	} else if (!strcmp(name, "cough_cough")) {
		sound = cough_cough;
	} else if (!strcmp(name, "game_start")) {
		sound = game_start;
	} else if (!strcmp(name, "inhale")) {
		sound = inhale;
	} else if (!strcmp(name, "reverse")) {
		sound = reverse;
	} else if (!strcmp(name, "ronf")) {
		sound = ronf;
	} else if (!strcmp(name, "rumble")) {
		sound = rumble;
	} else if (!strcmp(name, "sneeze_long")) {
		sound = sneeze_long;
	} else if (!strcmp(name, "sneeze_short")) {
		sound = sneeze_short;
	} else if (!strcmp(name, "game_over")) {
		sound = game_over;
	} else if (!strcmp(name, "in_game")) {
		sound = in_game;
	}
	
	return Mix_PlayChannel(-1, sound, loop);

}

void play_sound(char *name) {
	play_sound_internal(name, 0);
}

void play_sound_and_wait(char *name) {
	int chan = play_sound_internal(name, 0);
	int playing = Mix_Playing(chan);
	while(playing) { // wait for sound to end
		SDL_Delay(50);
		playing = Mix_Playing(chan);
	}
	SDL_Delay(500);
}

void play_bgm(char *name) {
	bgm_channel = play_sound_internal(name, -1);
}
void pause_bgm(void) {
	Mix_Pause(bgm_channel);
}
void resume_bgm(void) {
	Mix_Resume(bgm_channel);
}
void stop_bgm(void) {
	Mix_HaltChannel(bgm_channel);
}