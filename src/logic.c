#include "include.h"



void get_best_score_path(char* reference) {
    sprintf(reference, "%s/.jiljil/best_score", getenv("HOME"));
} 

// get_best_score_from_file loads the best saved score from best_score.txt and puts it to best_score
int get_best_score_from_file(void) {

    char best_score_path[64];
    get_best_score_path(best_score_path);

    if(access(best_score_path, F_OK) != 0) {
        printf("Couldn't read best score file at %s. This is normal if it's your first play.\n", best_score_path);
        errno = 0;
        char dirtomake[64];
        sprintf(dirtomake, "%s/.jiljil/", getenv("HOME"));
        if(mkdir(dirtomake, S_IRWXU) == -1) {
            switch (errno) {
                case EACCES :
                    printf("The home directory does not to allow write the .jiljil directory\n");
                case EEXIST:
                    printf("Pathname already exists\n");
                case ENAMETOOLONG:
                    printf("Pathname is too long\n");
                default:
                    perror("Error creating directory");
            }
        }
        return 0;
    }

    FILE *fp;
    char buff[10];
    fp = fopen(best_score_path, "r");
    fscanf(fp, "%s", buff);
    int bs;
    sscanf(buff, "%d", &bs);
    return bs;

}

// save_best_score_to_file puts the best score to best_score.txt
void save_best_score_to_file(int score) {

    char best_score_path[64];
    get_best_score_path(best_score_path);
    FILE *fp;

    int best_score = score;
    fp = fopen(best_score_path, "w");
    
    if(fp == NULL) {
        perror("Best score has NOT ben saved! Error opening file to save score");
        return;
    }
    
    char snum[5];
    sprintf(snum, "%d", best_score);
    fputs(snum, fp);
    fclose(fp);

}

float points_distance(int x1, int y1, int x2, int y2) {
	return sqrt(pow(x2- x1, 2) + pow(y2-y1, 2));
}

int is_player_inside_lemon(int player_x, int player_y, int lemon_x, int lemon_y) {
	return points_distance(player_x, player_y, lemon_x+32, lemon_y+32) < 32;
}


void entity_decelerate(float *speed_x, float *speed_y, float deceleration) {

    if(*speed_x > 0) {
        *speed_x -= deceleration;
    }else{
        *speed_x += deceleration;
    }
    if(*speed_y > 0) {
        *speed_y -= deceleration;
    }else{
        *speed_y += deceleration;
    }

}
void player_decelerate(float *speed_x, float *speed_y) {
    entity_decelerate(speed_x, speed_y, 0.1);
}
void lemon_decelerate(float *speed_x, float *speed_y) {
    entity_decelerate(speed_x, speed_y, 0.005);
    *speed_y += 0.05;
}

void entity_cap(float *speed_x, float *speed_y) {

    if(*speed_x > 3) {
        *speed_x = 3;
    }
     if(*speed_x < -3) {
        *speed_x = -3;
    }
     if(*speed_y > 3) {
        *speed_y = 3;
    }
    if(*speed_y < -3) {
        *speed_y = -3;
    }

}
void player_cap(float *speed_x, float *speed_y) {
    entity_cap(speed_x, speed_y);
}
void lemon_cap(float *speed_x, float *speed_y) {
    entity_cap(speed_x, speed_y);
}

// avoid player flickering because the speed is close to zero, but enough to make it move slightly
void player_approximate(float *player_speed_x, float *player_speed_y) {
    if(*player_speed_x > -0.1 && *player_speed_x < 0.1) {
        *player_speed_x = 0;
    }
    if(*player_speed_y > -0.1 && *player_speed_y < 0.1) {
        *player_speed_y = 0;
    }
}

void avoid_entity_outside_bonds(int *x, int *y, float *speed_x, float *speed_y, int br_x, int br_y) {
    if(*x < 20 || *x > br_x) {
        if(*x < 20) {
            *x = 20;
        }else{
            *x = br_x;
        }
        *speed_x = -*speed_x;
    }
    if(*y < 20 || *y > br_y) {
        if(*y < 20) {
            *y = 20;
        }else{
            *y = br_y;
        }
        *speed_y = -*speed_y;
    }
}
void avoid_player_outside_bonds(int *x, int *y, float *speed_x, float *speed_y) {
    avoid_entity_outside_bonds(x, y, speed_x, speed_y, 284, 204);
}
void avoid_lemon_outside_bonds(int *x, int *y, float *speed_x, float *speed_y) {
    avoid_entity_outside_bonds(x, y, speed_x, speed_y, 237, 157);
}

int check_player_inside_prints(int tail_x, int tail_y, int paw1_x, int paw1_y, int paw2_x, int paw2_y) {

    if(tail_x + 5 >= paw1_x && tail_x <= paw1_x + 10) {
        if(tail_y + 5 >= paw1_y && tail_y <= paw1_y + 10) {
            return 1;
        }
    }
    if(tail_x + 5 >= paw2_x && tail_x <= paw2_x + 10) {
        if(tail_y + 5 >= paw2_y && tail_y <= paw2_y + 10) {
            return 1;
        }
    }

    return 0;

}
