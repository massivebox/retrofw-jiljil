int get_best_score_from_file(void);
void save_best_score_to_file(int);

int is_player_inside_lemon(int, int, int, int);

void player_decelerate(float*, float*);
void player_cap(float*, float*);
void player_approximate(float*, float*);
void avoid_player_outside_bonds(int*, int*, float*, float*);

void lemon_decelerate(float*, float*);
void lemon_cap(float*, float*);
void avoid_lemon_outside_bonds(int*, int*, float*, float*);

int check_player_inside_prints(int, int, int, int, int, int);