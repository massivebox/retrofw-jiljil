#!/bin/sh

cd ipk
cd data && tar cvzf ../data.tar.gz *
cd ../control && tar cvzf ../control.tar.gz *
cd .. && ar rv build.ipk control.tar.gz data.tar.gz debian-binary
rm data.tar.gz control.tar.gz
rm -rf ./data/home/retrofw/games/jiljil-c/assets ./data/home/retrofw/games/jiljil-c/jiljil.dge
mv build.ipk ..