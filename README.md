# RetroFW JilJil

![1](https://cloud.massivebox.net/s/xBwkL7T7kzEW357/preview)![2](https://cloud.massivebox.net/s/axGycERyHpei3Sn/preview)![3](https://cloud.massivebox.net/s/mFN4y5sZecZYPSR/preview) 

[![status-badge](https://woodpecker.massivebox.net/api/badges/massivebox/retrofw-jiljil/status.svg)](https://woodpecker.massivebox.net/massivebox/retrofw-jiljil)

This repo contains the source code for my port of [JilJil](https://www.cavestory.org/pixels-works/jil-jil.php) for (hopefully) **RetroFW** compatible devices and **Linux**.

JilJil is a simple **arcade-style** score based game, where you control a weird **worm** inside an aquarium, and you have to run away from a **cat** that is trying to catch you by the ribbon on your tail. You also have to bounce off of a **lemon** as many times as you can.

While this game is named as a port of JilJil, the actual source code  of JilJil is not publicly available, so I decided to rewrite the game from scratch and I've added some tweaks here and there to  spice up the gameplay. The physics are not supposed to be 1:1 to actual  JilJil!

Some of the assets and resources used to create this game are (c) 1997 Studio Pixel. If you represent Mr. Daisuke Amaya, Studio Pixel, or any affiliates, please contact me at legal@massivebox.net.

## Make-ing

### On Linux

Simply run `make`.  
You'll need `SDL` development libraries (including SDL Mixer and SDL Image), and the C compiler `gcc`.  
You will get an executable called `2048.dge`, run it and enjoy your 2048 experience!  

### On Linux for RetroFW

Run `make -f Makefile.rfw ipk` to generate an IPK file, or `make -f Makefile.rfw opk` to generate an OPK (recommended).  
You will need the [RetroFW Buildroot](https://github.com/retrofw/buildroot-retrofw-2.3/tree/2021.11.x) to cross-compile. Check out my project [RetroFW Docker Buildroot](https://gitea.massivebox.net/massivebox/retrofw-docker-buildroot) to install the Buildroot in an easier and platform-independent way.

### On Windows or MacOS for RetroFW

Run `make -f Makefile.rfw ipk` to generate an IPK file, or `make -f Makefile.rfw opk` to generate an OPK (recommended).  
Since these platforms don't natively support Buildroot, I recommend you to use the [RetroFW Docker Buildroot](https://gitea.massivebox.net/massivebox/retrofw-docker-buildroot).

## Contributing

Contributions are always welcome! You can contribute by opening a pull request or an issue.  
You can also report me stuff via email if you don't want to sign up to whichever Git server I choose to host this project.  
My mail is hello@massivebox.net.

