CC := gcc
CFLAGS := `sdl-config --libs --cflags` -ggdb3 -O0 --std=c99 -Wall -lSDL_image -lSDL_mixer -lm
HDRS :=
SRCS := src/main.c src/match.c src/graphics.c src/logic.c src/keys.c src/audio.c
OBJS := $(SRCS:.c=.o)

EXEC := jiljil.dge

all: $(EXEC)

$(EXEC): $(OBJS) $(HDRS) Makefile
	$(CC) -o $@ $(OBJS) $(CFLAGS)

clean:
	rm -f $(EXEC) $(OBJS)

.PHONY: all clean
